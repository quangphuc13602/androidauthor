package com.example.ktralistview;

import java.io.Serializable;

public class Author implements Serializable {
    String nameAuthor;
    int imgAuthor;
    String work;
    String info;

    public Author(String nameAuthor, int imgAuthor, String work, String info) {
        this.nameAuthor = nameAuthor;
        this.imgAuthor = imgAuthor;
        this.work = work;
        this.info = info;
    }

    public Author(String work) {
        this.work = work;
    }

    public String getNameAuthor() {
        return nameAuthor;
    }

    public void setNameAuthor(String nameAuthor) {
        this.nameAuthor = nameAuthor;
    }

    public int getImgAuthor() {
        return imgAuthor;
    }

    public void setImgAuthor(int imgAuthor) {
        this.imgAuthor = imgAuthor;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}

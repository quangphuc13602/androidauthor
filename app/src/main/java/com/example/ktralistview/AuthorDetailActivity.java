package com.example.ktralistview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class AuthorDetailActivity extends AppCompatActivity {
    TextView tvName, back, tvInfo;
    ImageView imageViewAuthor;
    private WorkAdapter workAdapter;
    private List<Author> list;

    public AuthorDetailActivity() {
        list = new ArrayList<>();

            list.add(new Author("Tràng giang"));
            list.add(new Author("Đoàn thuyền đánh cá"));
            list.add(new Author("Ta viết bài thơ gọi biển về"));
            list.add(new Author("Ngậm ngùi"));
            list.add(new Author("Con chim chiền chiện"));

        workAdapter = new WorkAdapter(AuthorDetailActivity.this, list);

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_author_detail);

        Bundle bundle = getIntent().getExtras();
        if(bundle == null){
            return;
        }
        Author author = (Author) bundle.get("object_author");

        back = findViewById(R.id.tv_back_detail);
        back.setOnClickListener(view->{
            startActivity(new Intent(AuthorDetailActivity.this, MainActivity.class));
        });
        tvName = (TextView) findViewById(R.id.tv_name_detail);
        tvInfo = findViewById(R.id.infodetail);
        imageViewAuthor = (ImageView) findViewById(R.id.imageViewAuthor);
        ListView listWork = (ListView) findViewById(R.id.list_work);
        listWork.setAdapter(workAdapter);

        tvName.setText(author.getNameAuthor());
        tvInfo.setText(author.getInfo());
        imageViewAuthor.setImageResource(author.getImgAuthor());
    }
}
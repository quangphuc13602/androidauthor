package com.example.ktralistview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private AuthorAdapter authorAdapter;
    private List<Author> list;

    public MainActivity(){
        list = new ArrayList<>();

        list.add(new Author("Huy Can", R.drawable.huy_can,"Dat No Hoa", "Cù Huy Cận (1919 – 2005), bút danh hoạt động nghệ thuật là Huy Cận, là một chính khách, từng giữ nhiều chức vụ lãnh đạo cao cấp trong chính phủ Việt Nam như Bộ trưởng Bộ Canh nông (nay là Bộ Nông nghiệp và Phát triển nông thôn), Thứ trưởng Bộ Văn hóa Nghệ thuật, Bộ trưởng Bộ Văn hóa Giáo dục,"));
        list.add(new Author("Nam Cao", R.drawable.nam_cao,"Chi Pheo", "Nam Cao (1915 hoặc 1917[1] – 28 tháng 11 năm 1951) là một nhà văn và cũng là một chiến sĩ, liệt sĩ người Việt Nam. Ông là nhà văn hiện thực lớn (trước Cách mạng Tháng Tám), một nhà báo kháng chiến (sau Cách mạng), một trong những nhà văn tiêu biểu nhất thế kỷ 20. Nam Cao có nhiều đóng góp quan trọng đối với việc hoàn thiện phong cách truyện ngắn và tiểu thuyết Việt Nam ở nửa đầu thế kỷ 20."));
        list.add(new Author("Mac Ngon", R.drawable.mac_ngon,"Cao Luong Do", "Mạc Ngôn (sinh ngày 17 tháng 2 năm 1955) là một nhà văn người Trung Quốc xuất thân từ nông dân. Ông đã từng được thế giới biết đến với tác phẩm Cao lương đỏ đã được đạo diễn nổi tiếng Trương Nghệ Mưu chuyển thể thành phim. Bộ phim đã được giải Gấu vàng tại Liên hoan phim quốc tế Berlin năm 1988."));
        list.add(new Author("Nguyen Nhat Anh", R.drawable.nguyen_nhat_anh,"Mat Biec", "Nguyễn Nhật Ánh (sinh ngày 7 tháng 5 năm 1955 ở Thăng Bình, Quảng Nam)[1] là một nhà văn, nhà thơ, bình luận viên Việt Nam. Ông được biết đến qua nhiều tác phẩm văn học về đề tài tuổi trẻ, các tác phẩm của ông rất được độc giả ưa chuộng và nhiều tác phẩm đã được chuyển thể thành phim."));
        list.add(new Author("To Hoai", R.drawable.to_hoai,"De Men Phieu Luu Ki", "Tô Hoài (tên khai sinh: Nguyễn Sen; 27 tháng 9 năm 1920 – 6 tháng 7 năm 2014)[1] là một nhà văn Việt Nam. "));

        authorAdapter = new AuthorAdapter(this, list);

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView lv = findViewById(R.id.lvEmployees);
        lv.setAdapter(authorAdapter);
    }
}
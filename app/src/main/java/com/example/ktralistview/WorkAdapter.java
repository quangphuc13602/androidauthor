package com.example.ktralistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class WorkAdapter extends BaseAdapter {
    private Context context;
    private List<Author> list;
    public WorkAdapter(Context context, List<Author> list) {
        this.context = context;
        this.list = list;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        if (position < 0)
            return null;

        return list.get(position);    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.item_work, null);

        TextView tvname = view.findViewById(R.id.lvNameWork);


        Author author = list.get(position);

        tvname.setText(author.getWork());
        return view;
    }
}
